-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Sep 2020 pada 12.53
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajarblog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok DiSajikan saat HUT RI ke-75', 'blablabla', NULL, NULL, NULL, 'HUT RI Ke-75'),
(2, 'test', 'test', NULL, NULL, NULL, 'test'),
(3, 'Prof.', 'Dignissimos facere voluptates odio quae libero incidunt. Perspiciatis sapiente itaque rerum et aspernatur. Non commodi aliquid adipisci vel eum nulla.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Food Batchmaker'),
(4, 'Mrs.', 'In ipsa natus quod sunt qui iure dolor possimus. Corporis ut odit ea illum commodi.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Computer Support Specialist'),
(5, 'Mrs.', 'Recusandae et mollitia ut rerum velit. Assumenda necessitatibus qui illum dicta non. Laborum aut nisi ad occaecati. Ut facere occaecati ipsa recusandae ut.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Private Household Cook'),
(6, 'Mr.', 'Voluptatem sint accusamus labore incidunt labore. Corporis rerum dolore eos rem doloribus maiores. Est suscipit et labore recusandae nam aut aut. Repudiandae consequuntur vero enim molestiae dolorem quis.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Network Admin OR Computer Systems Administrator'),
(7, 'Miss', 'Velit doloribus commodi voluptatem id pariatur est doloribus. Sed sed voluptatem corrupti voluptatem minima. Sed deleniti deserunt non velit consequatur eius.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Mining Engineer OR Geological Engineer'),
(8, 'Prof.', 'Consectetur dolor quia eligendi blanditiis occaecati architecto sed. Fugit nihil assumenda sint eveniet qui distinctio recusandae.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Philosophy and Religion Teacher'),
(9, 'Dr.', 'Corporis distinctio neque sed asperiores rem accusamus doloribus. Non maiores alias voluptas consequatur recusandae. Ducimus quia quaerat facere unde.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Railroad Switch Operator'),
(10, 'Prof.', 'Sit officiis iste fugit aliquid et. Iusto labore qui aperiam magni reprehenderit occaecati ut dolor. Dolorem voluptatum fuga delectus voluptatem.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Aviation Inspector'),
(11, 'Prof.', 'Laborum reprehenderit similique enim laudantium odit dicta sint. Doloremque enim et sapiente dolor est quis. Dolorem et voluptatem suscipit consequatur facere beatae eum voluptatum. Sint molestias rerum omnis illo expedita cupiditate accusantium labore.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Carver'),
(12, 'Mr.', 'Ut nostrum voluptas doloribus iure dolorem a voluptatem. Totam dolor nulla sit non recusandae ipsum. Esse quis minus et adipisci. Ut officia repellat autem fuga.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Nuclear Technician'),
(13, 'Dr.', 'Dolore sequi ut est quia non iure. Et deserunt deleniti nesciunt sapiente. Enim occaecati suscipit atque enim.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Automotive Glass Installers'),
(14, 'Ms.', 'Assumenda et aut quae similique est incidunt. Molestiae quis corporis soluta dolorem.', '2020-08-19 02:58:36', '2020-08-19 02:58:36', NULL, 'Petroleum Pump System Operator'),
(15, 'Dr.', 'Et neque aut qui et illo id deserunt. Est distinctio accusantium molestias accusamus et. Optio mollitia consequuntur fugiat repudiandae a consequatur.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Parking Enforcement Worker'),
(16, 'Ms.', 'Quo soluta nobis nam et consequatur iusto velit. Nemo delectus iste est dicta expedita. Ipsa deleniti sunt dolorem vitae voluptatem commodi. Ipsa ut consequatur quas.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Grounds Maintenance Worker'),
(17, 'Prof.', 'Atque error magnam sapiente dignissimos deleniti omnis dolorem. Sint nihil quisquam in dolores. Sapiente non est reprehenderit nesciunt praesentium.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Law Teacher'),
(18, 'Miss', 'Est laboriosam minima sit reiciendis delectus vitae ullam maxime. Voluptatem totam maxime eos quasi id necessitatibus sunt quia. Ipsa ipsa laborum tempore error.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Museum Conservator'),
(19, 'Dr.', 'Inventore dolorem consectetur quidem placeat. Nobis id et nostrum dolor nihil. Velit omnis nemo ut voluptatem corporis. Et quia est autem et neque.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Transportation Manager'),
(20, 'Prof.', 'Est reiciendis deserunt repellat velit. Ea non accusamus eum atque dolores dolor. Distinctio est ut vitae a.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Auditor'),
(21, 'Mrs.', 'Animi quia repellat labore sit iure. Ipsa iste porro laboriosam qui consequatur. Magni molestiae fugiat et saepe. Tempore voluptates ut dolore sed voluptate eum.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Mapping Technician'),
(22, 'Dr.', 'At fuga minus sit ea eius quis voluptate sed. Qui sed qui voluptatum sequi et veritatis. Nesciunt fugiat temporibus eligendi eaque.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Director Of Talent Acquisition'),
(23, 'Ms.', 'Sit neque nisi voluptate qui nulla velit perferendis. Et porro dolorem sit soluta. Tempora minima consectetur suscipit quos fugit voluptatem. Consequatur expedita similique ipsa et sunt.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Architectural Drafter'),
(24, 'Prof.', 'Distinctio ea deserunt vel quia vitae nemo adipisci. Quia quidem omnis ea aut laborum. Sit commodi possimus sit autem aliquid illo.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Electrical and Electronics Drafter'),
(25, 'Dr.', 'Incidunt autem nemo numquam animi magni ea. Earum eligendi architecto reprehenderit earum temporibus. Blanditiis ut et architecto. Dolorem dolorum quidem odit cumque sed.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Forester'),
(26, 'Dr.', 'Magnam velit ut aut quas voluptatem facilis. Necessitatibus impedit similique fugiat et ducimus. Atque qui numquam repellendus rerum deserunt. Fugit sed similique quas.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Electrical Power-Line Installer'),
(27, 'Ms.', 'Fugiat et quia enim et necessitatibus corrupti dicta. Sunt nobis et ut. Aut accusamus occaecati adipisci aut. Dolor ullam quam rerum asperiores.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Financial Analyst'),
(28, 'Mr.', 'Sed voluptatem tenetur minima aut omnis exercitationem. Delectus asperiores aut totam soluta. Voluptatem assumenda qui quam ea molestiae qui.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Wellhead Pumper'),
(29, 'Ms.', 'Itaque non esse iure reiciendis molestias. Aperiam cum est voluptate non. Repellat commodi ex est nobis. Officia neque laudantium praesentium commodi ullam aliquid impedit.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Vocational Education Teacher'),
(30, 'Dr.', 'Enim dolores ut aut velit porro aut. Expedita consequatur totam non qui sint ut sed. Placeat optio vero rerum eius vero quisquam. Ut recusandae voluptas blanditiis debitis deserunt.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Forensic Science Technician'),
(31, 'Mrs.', 'Rerum ut fuga incidunt aspernatur accusamus aut excepturi. Molestiae voluptas mollitia modi tempora qui. Facilis est omnis tempore odio cum quia odio.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Record Clerk'),
(32, 'Ms.', 'Labore labore deserunt voluptatem aut. Ipsum quia asperiores nulla vel veniam non. Occaecati et cum sit harum rem blanditiis maxime.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Gaming Dealer'),
(33, 'Dr.', 'Neque quis omnis rerum. Molestias et dolorem temporibus id nesciunt. Amet vel nulla quos eligendi voluptatem dolores et a.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Traffic Technician'),
(34, 'Dr.', 'In quidem ut molestiae et omnis est autem dolor. Et incidunt nisi eaque mollitia quia eum. Quas ullam repellat quaerat ut eligendi dolore. Optio est in ex quo.', '2020-08-19 02:58:37', '2020-08-19 02:58:37', NULL, 'Fire-Prevention Engineer'),
(35, 'Miss', 'Autem a minus quidem voluptatibus commodi commodi saepe. Nihil maxime voluptatem est consequatur aspernatur. Minus neque at voluptatem delectus nobis aspernatur a.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Precision Lens Grinders and Polisher'),
(36, 'Mr.', 'Quia veritatis pariatur possimus aut eos. In dolores eos voluptatem et odio aut nemo. Quam harum eum corporis rerum. Inventore debitis et voluptatem est velit.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Electronics Engineer'),
(37, 'Mrs.', 'Expedita molestiae aspernatur rem doloribus sint. Voluptate eligendi ex est dolores eveniet facilis nam accusamus. Error doloremque aspernatur incidunt tempora. Omnis sunt dolorem non.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Occupational Therapist Assistant'),
(38, 'Mr.', 'Odio sint vel id sint fuga. Adipisci tenetur rem natus est. Id voluptatem eos repellat earum repudiandae perferendis vel. Sit in dolorem quam nostrum aut mollitia et.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Offset Lithographic Press Operator'),
(39, 'Miss', 'Expedita necessitatibus iusto aut fugiat nobis. Quo iusto quidem eos odio mollitia autem enim. Aut necessitatibus veritatis quidem voluptatem molestiae eum soluta.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Paralegal'),
(40, 'Dr.', 'Ullam voluptates error ut quia ex et. Doloremque assumenda assumenda et aut repellendus. Quia ad voluptatibus qui.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Heavy Equipment Mechanic'),
(41, 'Mr.', 'Hic repudiandae ut facere numquam dolore quia. Nobis sed voluptatum recusandae occaecati autem fugiat aut. Id iste voluptas vero optio consectetur earum ullam.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Coaches and Scout'),
(42, 'Prof.', 'Nisi quos corrupti quaerat soluta nam. Debitis porro provident repellendus consectetur minima sed. In consequatur officiis totam est dolorem libero veritatis eos. Sed odit similique voluptatem sed quae nulla omnis.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Kindergarten Teacher'),
(43, 'Prof.', 'Quia doloribus nam ut perferendis ut quam. Facere neque qui quaerat ea minus. Autem qui consequatur atque et nesciunt iure totam nobis. Natus aspernatur at quasi id perferendis.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Sociologist'),
(44, 'Dr.', 'Nulla totam praesentium expedita tenetur corrupti. Quas placeat quae in quibusdam harum. Impedit amet libero velit aut quam sequi.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Administrative Law Judge'),
(45, 'Prof.', 'Adipisci qui vel et libero nemo culpa sunt. At voluptatum quod totam deleniti molestiae velit sunt alias. Et velit sit est officiis. Omnis et optio odio perspiciatis.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Conveyor Operator'),
(46, 'Mr.', 'Perspiciatis nemo porro placeat dolorem. Esse minima harum cum et maxime dolorem. Et mollitia saepe laborum non.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Correspondence Clerk'),
(47, 'Prof.', 'Qui voluptas aliquid doloribus ad amet tenetur. Natus sit quaerat incidunt velit. Et error minima quas aut. Aut quia mollitia nobis sunt sit velit libero.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Rolling Machine Setter'),
(48, 'Mrs.', 'Molestiae fugit maiores eveniet vitae qui saepe. Maiores impedit maiores deserunt at dolore. Molestiae quia veniam libero labore.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Forest and Conservation Technician'),
(49, 'Miss', 'Veniam asperiores natus dolore. Repellendus sed dicta cum eos. Quas vel optio ipsam ratione ut cum. Ducimus impedit dolorem eveniet non odio beatae.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Pipelayer'),
(50, 'Prof.', 'Dicta dolore blanditiis voluptatem vel aperiam consequatur officiis. Facilis autem nihil consequatur aut. Recusandae et et at. Tenetur aperiam sint nihil omnis adipisci voluptatem et. Aut consequuntur nesciunt laudantium.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Compliance Officers'),
(51, 'Mr.', 'Enim aut error sed. Laudantium expedita qui enim alias distinctio. Rem necessitatibus eveniet eius eligendi ipsum.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Sociology Teacher'),
(52, 'Prof.', 'Est sint qui beatae eos impedit facilis. Ut molestiae tempora perspiciatis tempore quisquam consequatur. Voluptas possimus debitis illo quis. Soluta excepturi vitae aliquam possimus quidem. Eius assumenda ut quo maiores quisquam tempora qui.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Electro-Mechanical Technician'),
(53, 'Dr.', 'Velit enim delectus quis cumque suscipit laboriosam accusamus nisi. Non odit ut est est beatae sit. Sapiente maxime perspiciatis et. Magnam commodi perferendis adipisci et qui.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Administrative Support Supervisors'),
(54, 'Mr.', 'Optio dolorum consequuntur voluptatem est sapiente enim. Soluta numquam corporis ut laudantium. Ad vel nostrum adipisci debitis voluptas.', '2020-08-19 02:58:38', '2020-08-19 02:58:38', NULL, 'Fire Inspector'),
(55, 'Prof.', 'In dolor esse optio. Assumenda earum atque et quam.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Farm and Home Management Advisor'),
(56, 'Dr.', 'Impedit non sit voluptas porro qui ea. Placeat sed et distinctio dolor est. Ut qui consectetur nam vel est est eum.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Extraction Worker'),
(57, 'Dr.', 'Nostrum rerum quis dolores et. Est mollitia qui consectetur tempore ipsam vitae. Non ad rerum sint. Suscipit et tenetur fugiat vel ab.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Tractor-Trailer Truck Driver'),
(58, 'Miss', 'Reiciendis recusandae blanditiis omnis velit voluptate. Et et modi dolor earum unde vero explicabo. Veniam repudiandae et et neque et. Consequatur minus corporis reiciendis voluptatem aut vero consequatur. Necessitatibus cum optio similique saepe debitis quidem cupiditate.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Vice President Of Human Resources'),
(59, 'Prof.', 'Ex qui rerum quo facere. Minima aut aut minus et est quia alias. Omnis natus voluptatum qui eveniet delectus accusantium.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Gas Distribution Plant Operator'),
(60, 'Mr.', 'Tempore veniam praesentium et optio non autem. Quam quos quia ut. Qui quo beatae quos.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Manager Tactical Operations'),
(61, 'Prof.', 'Voluptatem debitis vitae ullam. Molestiae et quo ducimus qui harum sint. Enim qui corporis et ducimus ut. Consequuntur debitis in quam ut sunt qui.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Streetcar Operator'),
(62, 'Dr.', 'Maiores quam deserunt et dolor. Quia sed est est dolorem. Enim vero ut ut in esse veritatis consectetur. Molestiae rerum rerum aut vel.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Real Estate Appraiser'),
(63, 'Mr.', 'Sit minima doloremque itaque non est quas libero. Rerum est eum repellat id at qui.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Auditor'),
(64, 'Dr.', 'Repudiandae voluptatem magni illum accusamus cupiditate sapiente. Ut voluptas rerum vel id pariatur qui.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Airline Pilot OR Copilot OR Flight Engineer'),
(65, 'Mr.', 'Nihil omnis saepe corporis numquam. Quam fuga error consequatur esse tempora ut dolorem. Odit voluptatem odio molestiae dolorem.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Fire Investigator'),
(66, 'Prof.', 'Maiores excepturi magnam ab magni modi neque nisi. Dicta neque in qui vero occaecati exercitationem eum. Eum sunt minima labore pariatur in autem. Nemo odit rerum architecto in.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Loan Counselor'),
(67, 'Mr.', 'Voluptatem et aut aut. Repellendus ratione ipsum fuga sapiente modi corporis. Commodi quaerat reprehenderit facilis ut cumque. Et non quo debitis molestias.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Microbiologist'),
(68, 'Dr.', 'Est quia dignissimos ut repellat placeat sed voluptatem. Sed nobis tempore iure. Recusandae adipisci ullam necessitatibus consequatur dicta tempora. Voluptatem accusamus harum quisquam reiciendis accusamus.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Correctional Officer'),
(69, 'Prof.', 'Eius sed maxime occaecati harum voluptatem voluptate quis. Fugit dolor sed rem et. Id quia id fugiat consequatur. Nesciunt nisi facere sequi molestiae.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Separating Machine Operators'),
(70, 'Prof.', 'Magni voluptatibus magni eos fuga. Et enim ducimus enim rerum. Eligendi sequi facilis quis esse deserunt.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Tractor Operator'),
(71, 'Prof.', 'Nemo hic repellendus dolorum rerum. Incidunt explicabo quibusdam omnis amet fuga eum commodi. Quaerat quia qui est et ipsam.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Civil Engineering Technician'),
(72, 'Ms.', 'Maiores totam quo quibusdam magnam modi ipsa. Odit quos quia qui eius saepe et velit.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Bicycle Repairer'),
(73, 'Dr.', 'Architecto ipsa accusantium minima non rem sunt accusantium maiores. Possimus accusamus porro est quam at esse. Eius assumenda et illo ipsa quisquam cupiditate. Atque quis ullam veniam adipisci quo qui.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Construction'),
(74, 'Mr.', 'Odit nihil iusto eaque aut. Culpa quia quibusdam magni quis. Vel placeat corrupti repellat rem unde.', '2020-08-19 02:58:39', '2020-08-19 02:58:39', NULL, 'Personnel Recruiter'),
(75, 'Dr.', 'At saepe explicabo nobis tenetur cum qui. Laudantium totam magnam ut corporis et rerum et.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Soldering Machine Setter'),
(76, 'Prof.', 'Non repellendus facere minus veniam labore dolorem. Ad sint ut asperiores eius quia quibusdam sapiente. Quis nostrum qui et qui iste laudantium ea. Non et recusandae tempora eveniet.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Electronic Equipment Assembler'),
(77, 'Prof.', 'Blanditiis alias quo adipisci ad qui ratione alias. Quae quibusdam voluptatem sapiente ut. Est et accusantium laboriosam quae quia.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Compacting Machine Operator'),
(78, 'Mr.', 'Magnam accusamus fugiat sed labore. Vitae assumenda et sapiente dolor error nihil totam. Occaecati recusandae inventore sed est. Ut occaecati quia non voluptatem.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Political Scientist'),
(79, 'Dr.', 'Est perspiciatis veritatis possimus labore. Voluptatem error molestiae labore temporibus dignissimos explicabo similique. Velit sed quis neque ut sint ea. Id iure laborum optio consequatur in quaerat non.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Sales Representative'),
(80, 'Miss', 'Ipsam repudiandae distinctio iusto sint. Totam vel ut nesciunt iure eum totam esse. Rerum non adipisci aut sit.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'University'),
(81, 'Prof.', 'Exercitationem omnis quisquam iure quae esse. Rerum et et reiciendis cupiditate voluptatem. Consequatur laboriosam quia reprehenderit temporibus. Cumque animi voluptas rerum consequatur dolorem harum quasi.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Housekeeping Supervisor'),
(82, 'Mrs.', 'Natus dicta commodi numquam. Porro aut minima totam consequatur repellat neque dicta. Rem architecto qui est delectus amet nemo. Quaerat ipsam ab autem dolorum quia.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Electrician'),
(83, 'Prof.', 'Odio quis rerum recusandae eveniet. Veniam ipsum sint quae et alias eligendi. Accusantium fugit rerum qui ipsum sit.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Brazer'),
(84, 'Prof.', 'Dolores dolore repellat animi porro exercitationem doloremque. Labore dicta quia voluptatem nobis et consequatur incidunt. Ut ut deserunt velit dicta inventore.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Artillery Officer'),
(85, 'Dr.', 'Numquam sequi sunt non ut blanditiis voluptates temporibus. Et dolore quia et. Eos et voluptas vel ipsam eius.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Cutting Machine Operator'),
(86, 'Prof.', 'Veniam qui itaque optio modi. Est aliquid harum quaerat. Id ipsum magnam soluta laudantium sed dicta alias. Quas qui quia ut excepturi aspernatur. Aspernatur vitae deserunt sequi odit.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Construction Manager'),
(87, 'Prof.', 'Labore eaque ut officia rerum. Dolorem accusantium qui eius reprehenderit voluptatem laudantium quidem. Cupiditate ut et quibusdam quia. Ex quia eligendi eius. Aut voluptatem fuga delectus quia modi.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Historian'),
(88, 'Dr.', 'Numquam fuga inventore quos aspernatur qui. Esse voluptatem laborum tempora dolores provident porro quos reiciendis. Rerum in modi inventore aut. Voluptatibus asperiores deserunt et incidunt.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Sawing Machine Setter'),
(89, 'Miss', 'Rerum explicabo placeat reiciendis veritatis soluta. Voluptas cum commodi facere non. Ut expedita sint rerum qui corrupti. Nam aut in perferendis nesciunt quas voluptas.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'First-Line Supervisor-Manager of Landscaping, Lawn Service, and Groundskeeping Worker'),
(90, 'Mr.', 'Neque sint odit possimus doloribus alias enim reiciendis. Esse quas impedit accusamus nostrum voluptatum ea harum odio. Voluptatem distinctio corporis quo praesentium. Impedit sit amet neque veniam praesentium.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Child Care'),
(91, 'Prof.', 'Veniam nemo et magnam sed et accusamus est doloremque. Itaque nam qui vel natus aspernatur assumenda. Atque tenetur explicabo doloribus ipsa quidem.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Gas Distribution Plant Operator'),
(92, 'Dr.', 'Iure non sed ut velit aut explicabo asperiores. Dolor repellendus voluptas esse. Illo magni dolor earum incidunt. Et quisquam amet nobis. Voluptas et nihil rerum deleniti nobis repellendus nulla ut.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Data Entry Operator'),
(93, 'Miss', 'Repellendus sed culpa nihil earum sed. Autem officiis id quisquam non soluta sequi. Quidem aliquam accusamus veniam hic ut quibusdam. Nemo perferendis inventore nihil quis.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Political Scientist'),
(94, 'Mr.', 'Tempore nulla veritatis voluptas quaerat non laudantium optio modi. Dolore aspernatur dolores necessitatibus fuga. Est neque nobis fugit eligendi qui aliquid suscipit. Voluptatum voluptas aspernatur architecto omnis sed omnis excepturi omnis. Quia quas vel quisquam rerum totam corrupti consequuntur.', '2020-08-19 02:58:40', '2020-08-19 02:58:40', NULL, 'Mixing and Blending Machine Operator'),
(95, 'Prof.', 'Fugit doloremque et enim autem dolorem maiores est magnam. Corporis vitae dolor rem ut qui dolores qui omnis. Et esse et et omnis velit ratione ut consequatur.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Electrician'),
(96, 'Prof.', 'Sint voluptas dolor animi magni placeat quae. Maiores natus et beatae dolores ex. Quaerat et voluptatem sit sequi.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Physical Therapist Aide'),
(97, 'Ms.', 'Sapiente deleniti ad expedita ex quisquam. Et architecto autem ab et sed aut ut. Harum eos ducimus qui voluptates necessitatibus.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Business Manager'),
(98, 'Mr.', 'Dolorem molestias laborum omnis enim vel explicabo quo sequi. Nam beatae magni possimus odit illo. Dolore distinctio dicta quis dolores. Aut repellendus et nostrum aperiam. Qui error exercitationem a.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Recreational Therapist'),
(99, 'Mrs.', 'Dicta et ut sit sed rerum pariatur. Qui rerum aut omnis inventore. Doloremque qui neque voluptatem exercitationem.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Construction Carpenter'),
(100, 'Dr.', 'Qui est quam est occaecati. Voluptatem distinctio dolore quibusdam dicta aut quia.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Prosthodontist'),
(101, 'Miss', 'Et corrupti voluptas voluptas architecto consequatur et aut. Qui aut et reiciendis ipsum tempora. Natus eos officia voluptatem voluptatem quaerat. Dolore aliquam aliquam est impedit odio et sed.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Power Distributors OR Dispatcher'),
(102, 'Mrs.', 'Repudiandae vel quia sed atque. Quam est iure quidem ab numquam veritatis sit tempora. Accusantium quae quas quidem et. Quis officia qui quis possimus quia iusto.', '2020-08-19 02:58:41', '2020-08-19 02:58:41', NULL, 'Cost Estimator');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(11, '2014_10_12_000000_create_users_table', 1),
(12, '2014_10_12_100000_create_password_resets_table', 1),
(13, '2019_08_19_000000_create_failed_jobs_table', 1),
(14, '2020_08_15_055458_create_blog_table', 1),
(15, '2020_08_15_062312_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
